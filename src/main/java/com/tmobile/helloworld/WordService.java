package com.tmobile.helloworld;

import java.util.HashMap;
import java.util.Map;

public class WordService {

    public Map<String, Integer> count(String inputString){
        String [] inputArray = inputString.split(" ");
        Map<String, Integer> countMap = new HashMap();
        for (String localString : inputArray) {
            Integer prevNum = countMap.get(localString);
            if (prevNum == null) {
                countMap.put(localString, 1);
            } else {
                prevNum++;
                countMap.put(localString, prevNum);
            }
        }
        return countMap;
    }
}
