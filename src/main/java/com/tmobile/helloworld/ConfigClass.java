package com.tmobile.helloworld;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ConfigClass {

    @Bean
    public WordService getWordService() {
        return new WordService();
    }

}
