package com.tmobile.helloworld;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@RequestMapping("/words")
public class WordController {

    @Autowired
    WordService wordService;

    @PostMapping("/count")
    public Map<String, Integer> countWords(@RequestBody String requestBody){
        return wordService.count(requestBody);
    }
}
